#!/bin/bash

aws cloudformation package --profile mfa --region us-east-2 --template portfolio.yaml --s3-bucket jack-ross-training-bluegreen --output-template-file generatedTemplate.yaml

read -r -p "Continue? [Y/n] " input
case $input in
    [nN][oO]|[nN])
        exit 1
       ;;
esac

aws cloudformation deploy --profile mfa --region us-east-2 --template-file generatedTemplate.yaml --stack-name jack-ross-service-catalog --capabilities CAPABILITY_NAMED_IAM #--parameter-overrides $(cat params)

rm generatedTemplate.yaml
