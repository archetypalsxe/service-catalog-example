#!/bin/bash

aws servicecatalog create-product --profile mfa --region us-east-2 \
    --cli-input-json file://products/simple-apache.json \
    --name jack-ross-simple-apache \
    --owner jack.ross.labs \
    --product-type CLOUD_FORMATION_TEMPLATE \
    --provisioning-artifact-parameters
